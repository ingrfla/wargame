package warGame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class InfantryUnitTest {
    InfantryUnit infantryUnit,opponent,infantryUnitForest;

    @BeforeEach
            public void reset() {
        try {
             infantryUnit = new InfantryUnit("infantary", 55);
             opponent = new InfantryUnit("infantaryO", 20);
            infantryUnitForest = new InfantryUnit("infantary", 55);
            infantryUnitForest.setTerrain(Terrain.FOREST);
            opponent.setTerrain(Terrain.FOREST);
        }
catch (Exception e){
    System.out.println("error");}
    }
   @Test
   public void attackBonusTest(){
       assertEquals(2,infantryUnit.getAttackBonus());
   }



    @Test
    public void attakBonusForest(){
        assertEquals(4,infantryUnitForest.getAttackBonus());
    }

    @Test
    public void attackBonusDeafult(){
        assertEquals(2,infantryUnit.getAttackBonus());
    }
@Test
public void  resistBonusTestForest(){
        assertEquals(4,infantryUnitForest.getResistBonus());
}

    @Test
    public void  resistBonusTestDeafult(){
        assertEquals(1,infantryUnit.getResistBonus());
    }


    /**
     * opponent.health-(this.attack + this.getAttackBonus()) + (opponent.getArmour() + getResistBonus());
     * expected value= 20-(15+2) + (10+1)=14
     */

    @Test
    void healthTest() {
        infantryUnit.attack(opponent);
        assertEquals(14, opponent.getHealth());
    }

        @Test
    public void healthAfterAttackForest(){
        infantryUnitForest.attack(opponent);
        assertEquals(opponent.getHealth(),15);
        }
}


