package warGame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

class RangedUnitTest {
RangedUnit rangedUnit, opponentRanged, rangedUnitForest, rangedUnitHill;

    /**
     * New instance of RangedUnit
     */
    @BeforeEach
    public void reset() {

        try {
            rangedUnit = new RangedUnit("Ranged", 40);
            opponentRanged = new RangedUnit("opponentRanged", 35);
            rangedUnitForest = new RangedUnit("rangedUnitForest",50);
            rangedUnitForest.setTerrain(Terrain.FOREST);
            rangedUnitHill = new RangedUnit("rangedUnitForest",50);
            rangedUnitHill.setTerrain(Terrain.HILL);


        }





        catch (Exception e){
            System.out.println("error");}
    }



    /**
     * Test to check if resist bonus  in Ranged Unit gives the excepted values
     */
    @Test
    public void BonusTest() {
        assertEquals(6,rangedUnit.getResistBonus());

    }

    @Test
    public void BonusTest2(){
        rangedUnit.getResistBonus();
        assertEquals(4, rangedUnit.getResistBonus());

    }
    @Test
    public void BonusTest3(){
        rangedUnit.getResistBonus();
        rangedUnit.getResistBonus();
        rangedUnit.getResistBonus();
        assertEquals(2, rangedUnit.getResistBonus());

    }


    /**
     * Test to check if health is correct in attack method
     * opponent.health-(this.attack + this.getAttackBonus()) + (opponent.getArmour() + getResistBonus());
     * First expected value = 35-(15+3) + (8+6)=31
     * Second expected value = 31-(15+3)+(8+4) =25
     * Third expected value = 25- (15+3)+(8+2) = 17
     * Fourth expected value = 17- (15+3)+(8+2)= 13
     */
    @Test
    void healthTest() {
        rangedUnit.attack(opponentRanged);
        assertEquals(31, opponentRanged.getHealth());
    }

    @Test
    void healthTest2(){
        rangedUnit.attack(opponentRanged);
        rangedUnit.attack(opponentRanged);
        assertEquals(25, opponentRanged.getHealth());
    }

    @Test
    void healthTest3(){
        rangedUnit.attack(opponentRanged);
        rangedUnit.attack(opponentRanged);
        rangedUnit.attack(opponentRanged);
        assertEquals(17, opponentRanged.getHealth());
    }

    @Test
    void healthTest4(){
        rangedUnit.attack(opponentRanged);
        rangedUnit.attack(opponentRanged);
        rangedUnit.attack(opponentRanged);
        rangedUnit.attack(opponentRanged);
        assertEquals(9, opponentRanged.getHealth());
    }


@Test
    public void forestAttackBonus(){

            assertEquals(2,rangedUnitForest.getAttackBonus());
}


    @Test
    public void hillAttackBonus(){
        assertEquals(5,rangedUnitHill.getAttackBonus());
    }

    @Test
    public void defaultAttackBonus(){
         assertEquals(3,rangedUnit.getAttackBonus());
    }

}

