package warGame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class CavalaryUnitTest {
    CavalryUnit cavalryUnit,opponent,cavarlyUnitForest,cavarlyUnitPlain;
    /**
     * New instance of CalvaryUnit
     */  @BeforeEach
    public void reset() {
        try {
            cavalryUnit = new CavalryUnit("Calvary", 15);
            opponent = new CavalryUnit("opponent", 60);
             cavarlyUnitForest = new CavalryUnit("Forest", 15);
            cavarlyUnitPlain = new CavalryUnit("Plains", 15);
            cavarlyUnitForest.setTerrain(Terrain.FOREST);
            cavarlyUnitPlain.setTerrain(Terrain.PLAINS);
        }
    catch (Exception e){
        System.out.println("error");}
    }

    /**
     * Test to check if attack bonus in CalvaryUnit gives the excepted values
     */
    @Test
    public void resistBonusForest(){
        assertEquals(0,cavarlyUnitForest.getResistBonus());
    }
    @Test
    public void defaultResistBonus(){
        assertEquals(1,cavalryUnit.getResistBonus());
    }
    @Test
    public void attackBonus(){
        assertEquals(4,cavalryUnit.getAttackBonus());

}
@Test
public void attackBonusSecondAttack(){
        cavalryUnit.getAttackBonus();
    assertEquals(2,cavalryUnit.getAttackBonus());
}

    @Test
    public void attackBonusPlain(){
        assertEquals(5,cavarlyUnitPlain.getAttackBonus());
    }

    @Test
    public void attackBonusPlainSecondAttack(){
        cavarlyUnitPlain.attack(opponent);
        assertEquals(3,cavarlyUnitPlain.getAttackBonus());
    }

    /**
     * Test to check health
     *opponent.health-(this.attack + this.getAttackBonus()) + (opponent.getArmour() + getResistBonus());
     * expected first time= 60-(20+4)+(12+1)=49
     * expected second time = 49-(20+2)+(12+1)= 40
     * expected third time = 40-(20+2)+(12+1)= 31
     */
    @Test
    public void healthTest() {
        cavalryUnit.attack(opponent);
        assertEquals(49, opponent.getHealth());
    }
    @Test
    public void healthTestSecondAttack(){
        cavalryUnit.attack(opponent);
        cavalryUnit.attack(opponent);
        assertEquals(40,opponent.getHealth());
    }

    @Test
    public void healthTestThirdAttack(){
        cavalryUnit.attack(opponent);
        cavalryUnit.attack(opponent);
        cavalryUnit.attack(opponent);
        assertEquals(31,opponent.getHealth());
    }





}
