package warGame;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

import static org.junit.jupiter.api.Assertions.*;

public class ArmyTest {
    Army army;
    Unit unit1, unit2, infantaryunit, commanderunit, rangedunit;

    @BeforeEach
    public void reset() {
        army = new Army("Test");
        unit1 = new CavalryUnit("TesCalvarytUnit", 30);
        unit2 = new CavalryUnit("testCalvaryUnit", 45);
        infantaryunit = new InfantryUnit("TestInfantaryUnit", 10);
        commanderunit = new CommanderUnit("TestCommanderUnit", 10);
        rangedunit = new RangedUnit("RangedUnit", 10);

    }

    /**
     * test to check if getName returns the correct name
     */
    @Test
    void correctNameTest() {
        String name = "Army";
        Army army = new Army(name, new ArrayList<>());

        assertEquals(name, army.getName());
    }

    /**
     * test to check if the unit is added to the right army
     */
    @Test
    void addUnitTest() {
        army.addUnit(unit1);

        assertTrue(army.hasUnits());
    }


    /**
     * test to check if the method addUnits adds the units
     */
    @Test
    void addUnitsTest() {
        ArrayList<Unit> units = new ArrayList<>();
        assertEquals(army.getUnits().size(), 0);
        units.add(unit1);
        units.add(unit2);
        army.addUnits(units);
        assertEquals(army.getUnits().size(), 2);

    }

    /**
     * test to check if the method removeUnit removes the correct unit
     */
    @Test
    void RemoveUnitTest() {
        army.addUnit(unit1);
        assertEquals(army.getUnits().size(), 1);
        army.removeUnit(unit1);
        assertFalse(army.hasUnits());

    }

    /**
     * test to check if the method hasUnits() returns the right boolean, either true or false
     */
    @Test
    void hasUnitTest() {
        assertFalse(army.hasUnits());
        assertEquals(army.getUnits().size(), 0);

        army.addUnit(unit1);

        assertTrue(army.hasUnits());
    }

    /**
     * test to check if getUnits returns all units from Army class
     */
    @Test
    void getUnitsTest() {
        ArrayList<Unit> units = new ArrayList<>();
        units.add(unit1);
        units.add(unit2);

        army.addUnits(units);

        assertEquals(army.getUnits().size(), 2);

    }

    @Test
    void infantaryUnitTest() {
        ArrayList<Unit> unit = new ArrayList<>();
        unit.add(unit1);
        unit.add(infantaryunit);
        army.addUnits(unit);
        assertEquals(army.getInfantaryUnit().size(), 1);

    }

    @Test
    void commanderUnitTest() {
        ArrayList<Unit> unit = new ArrayList<>();
        unit.add(infantaryunit);
        unit.add(commanderunit);
        army.addUnits(unit);
        assertEquals(army.getCommanderUnit().size(), 1);


    }


    @Test
    void rangedUnitTest(){
        ArrayList<Unit> unit = new ArrayList<>();
        unit.add(infantaryunit);
        unit.add(rangedunit);
        army.addUnits(unit);
        assertEquals(army.getRangedUnits().size(), 1);
    }


    @Test
    void calvaryUnitTest(){
        ArrayList<Unit> unit = new ArrayList<>();
        unit.add(infantaryunit);
        unit.add(unit1);
        unit.add(commanderunit);
        army.addUnits(unit);
        assertEquals(army.getCavlaryUnits().size(), 1);
    }
}



