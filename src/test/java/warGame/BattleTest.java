package warGame;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class BattleTest {
    /**
     * Test to check is the simulation method in Battle works. Different units are added to
     * army one and army two with a for loop. AssertTrue checks if the test passed
     */

    @Test
    public void simulateTest() {
        Army humans = new Army("Human Army");
        Army orcs = new Army("Orcish horde");

        for (int i = 0; i < 500; i++) {
            humans.addUnit(new InfantryUnit("Footman", 100));
            orcs.addUnit(new InfantryUnit("Grunt", 100));
        }
        for (int i = 0; i < 200; i++) {
            humans.addUnit(new CavalryUnit("Knight", 100));
            orcs.addUnit(new CavalryUnit("Raider", 100));
        }
        for (int i = 0; i < 100; i++) {
            humans.addUnit(new RangedUnit("Archer", 100));
            orcs.addUnit(new RangedUnit("Spearman", 100));
        }
        for (int i = 0; i < 1; i++) {
            humans.addUnit(new CommanderUnit("Mountain King", 180));
            orcs.addUnit(new CommanderUnit(" Gul´dan", 180));
        }
        Battle battle = new Battle(humans, orcs,Terrain.HILL);
        assertTrue(battle.simulate().equals(humans) || battle.simulate().equals(orcs));


    }


}










