package warGame;

import org.junit.jupiter.api.Test;

public class ArmyWriteToFileTest {


    @Test
    void army() {
        Army humans = new Army("Human Army");
        for (int i = 0; i < 2; i++) {
            humans.addUnit(new InfantryUnit("Footman", 100));
        }
        for (int i = 0; i < 2; i++) {
            humans.addUnit(new CavalryUnit("Knight", 100));
        }
        for (int i = 0; i < 2; i++) {
            humans.addUnit(new RangedUnit("Archer", 100));
        }
        for (int i = 0; i < 1; i++) {
            humans.addUnit(new CommanderUnit("Mountain King", 180));

        }
        FileManagement.writeArmyToFile(humans);
    }


        @Test
        void armyFactory(){
            UnitFactory factory = new UnitFactory();
            Army army = new Army("PROGRAMMERS");
            army.addUnits(factory.listOfUnits(10,UnitEnum.INFANTRY,"Footman",100));
            army.addUnits(factory.listOfUnits(10,UnitEnum.CAVALRY,"Knight",100));
          army.addUnits( factory.listOfUnits(10,UnitEnum.COMMANDER,"Archer",100));
          army.addUnits( factory.listOfUnits(10,UnitEnum.RANGED,"Mountain King",100));
          FileManagement.writeArmyToFile(army);
        }




    }









