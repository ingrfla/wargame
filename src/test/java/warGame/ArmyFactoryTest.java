package warGame;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;

public class ArmyFactoryTest {



        RangedUnit rangedUnit;
        InfantryUnit infantryUnit;
        CavalryUnit cavalryUnit;
        CommanderUnit commanderUnit;
    ArrayList<Unit> infantaryunits;
        UnitFactory unitFactory = new UnitFactory();

        @BeforeEach
         void factorySetUp() {
            infantryUnit = (InfantryUnit) unitFactory.makeUnit(UnitEnum.INFANTRY,"infantary",10);
            rangedUnit = (RangedUnit) unitFactory.makeUnit(UnitEnum.RANGED,"Ranged",20);
            cavalryUnit = (CavalryUnit) unitFactory.makeUnit(UnitEnum.CAVALRY,"Cavalry",10);
             commanderUnit = (CommanderUnit) unitFactory.makeUnit(UnitEnum.COMMANDER,"Commander",10);
         infantaryunits= unitFactory.listOfUnits(10,UnitEnum.INFANTRY,"Infatries",10);
        }


        @Test
        void testCorrectTypeRanged(){
            Assertions.assertEquals(UnitEnum.RANGED,rangedUnit.getUnitType());
        }

    @Test
    void testCorrectTypeInfantary(){
        Assertions.assertEquals(UnitEnum.INFANTRY,infantryUnit.getUnitType());
    }

    @Test
    void testCorrectTypeCavlary(){
        Assertions.assertEquals(UnitEnum.CAVALRY,cavalryUnit.getUnitType());
    }
    @Test
    void testCorrectTypeCommander(){
        Assertions.assertEquals(UnitEnum.COMMANDER,commanderUnit.getUnitType());
    }


    @Test
    @DisplayName("Throw  Exception when name is null")
    void nameNullException(){
            Assertions.assertThrows(NullPointerException.class,()->unitFactory.makeUnit(UnitEnum.INFANTRY,null,10));
    }

    @Test
    void illegalArgument() {
        Assertions.assertThrows(IllegalArgumentException.class, () -> unitFactory.makeUnit(UnitEnum.INFANTRY, "Illegal", -5));
    }

    @Test
    void testInfantary() {
        for (int i = 0; i < infantaryunits.size(); i++) {
            infantaryunits.get(i).getUnitType();
            Assertions.assertEquals(UnitEnum.INFANTRY, infantaryunits.get(i).getUnitType());
        }
    }

    @Test
    void testNumber(){
            Assertions.assertEquals(10,infantaryunits.size());
    }

    @Test
    void checkNumber(){
            Assertions.assertEquals(10,infantaryunits.size());
    }


}
