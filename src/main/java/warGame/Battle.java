package warGame;

public class Battle {
    private final Army armyOne;
    private final Army armyTwo;



    /**
     * Constructor of a batte
     *
     * @param armyOne of the battle
     * @param armyTwo of the battle
     */
    public Battle(Army armyOne, Army armyTwo,Terrain terrain) {
        this.armyOne = armyOne;
        this.armyTwo = armyTwo;
       armyOne.getUnits().forEach(e->e.setTerrain(terrain));
        armyTwo.getUnits().forEach(e->e.setTerrain(terrain));

    }


    /**
     * Method to simulate the battle of two armies
     *
     * @return ArmyOne or ArmyTwo, depending on who wins
     */
    public Army simulate() {

        while (armyOne.hasUnits() && armyTwo.hasUnits()) {

            Unit unitOne = armyOne.getRandom();
            Unit unitTwo = armyTwo.getRandom();

            armyOne.getRandom().attack(unitTwo);
            if (unitTwo.getHealth() <= 0) {
                armyTwo.removeUnit(unitTwo);
            }
            if (!armyTwo.hasUnits()) {
                break;
            }
            armyTwo.getRandom().attack(unitOne);
            if (unitOne.getHealth() <= 0) {
                armyOne.removeUnit(unitOne);
            }
        }
        if (!armyTwo.hasUnits()) {
            return armyOne;

        } else {
            return armyTwo;
        }
    }




    /**
     * To string of battle
     *
     * @return armu one and army two
     */

    @Override
    public String toString() {
        return "Battle:     " +
                "Army number One" + armyOne +
                "Army number Two" + armyTwo;
    }
}