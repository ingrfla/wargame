package warGame;

public interface UnitType {
    UnitType getUnitType(Unit u);
}
