package warGame;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Random;


public class Army  {
    private final String name;
    List<Unit> units = new ArrayList<Unit>();

    /**
     * constructor that creates an army based on the name
     *
     * @param name of the unit
     */
    public Army(String name) {
        this.name = name;
        this.units = units;
    }

    /**
     * Constructor of an army with name and units
     *
     * @param name  of the army
     * @param units of the army
     */
    public Army(String name, List<Unit> units) {
        this.name = name;
        this.units = units;
    }


    /**
     * Name method for army
     *
     * @return the name of the armu
     */
    public String getName() {
        return name;
    }

    public String setName(String line){return name;}


    /**
     * method to add an unit
     *
     * @param unit takes in unit to add
     * @return the list with the unit added
     */
    public boolean addUnit(Unit unit) {
        if (units.contains(unit)) return false;
        return units.add(unit);
    }


    /**
     * Method to add units to the list
     *
     * @param units takes in the list as parameter
     * @return true
     */

    public boolean addUnits(List<Unit> units) {
        this.units.addAll(units);
        return true;
    }


    /**
     * method to remove an unit
     *
     * @param unit
     * @return false if the list does not contain the unit
     */
    public boolean removeUnit(Unit unit) {
        units.remove(unit);
        return false;
    }


    /**
     * Method to get units
     *
     * @return the list with units
     */
    public List<Unit> getUnits() {
        return units;
    }


    /**
     * Method to check if an army has units
     *
     * @return true if it is not empty and false if it does not have units
     */
    public boolean hasUnits() {
        return !units.isEmpty();
    }


    /**
     * method to pick a random unit
     *
     * @return the random unit
     */
    public Unit getRandom() {
        Random unit = new Random();
        return units.get(unit.nextInt(units.size()));
    }


    /**
     * method to check equality
     *
     * @param o the object o
     * @return the equality check
     */

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Army)) return false;
        Army army = (Army) o;
        return Objects.equals(getName(), army.getName()) && Objects.equals(units, army.units);
    }

    /**
     * method that returns the hashcode value
     *
     * @return the hashcode value
     */
    @Override
    public int hashCode() {
        return Objects.hash(getName(), units);
    }


    /**
     * Text with name of army and units
     *
     * @return name of army and units
     */




    @Override
    public String toString() {
        return "Army" + name + " units: " + units;
    }


    public List<Unit> getInfantaryUnit() {

        return this.units.stream().filter(unit -> unit instanceof InfantryUnit).toList();
    }
    public List<Unit> getCavlaryUnits() {

        return this.units.stream().filter(calvary -> calvary instanceof CavalryUnit).filter(calvary ->!(calvary instanceof CommanderUnit)).toList();
    }

    public List<Unit> getRangedUnits() {

        return this.units.stream().filter(unit -> unit instanceof RangedUnit).toList();
    }
    public List<Unit> getCommanderUnit() {

        return this.units.stream().filter(unit -> unit instanceof CommanderUnit).toList();
    }
}





