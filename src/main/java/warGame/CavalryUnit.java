package warGame;

public class CavalryUnit extends Unit {


    /**
     * Constructor extended from super- class Unit
     *
     * @param name   of the unit
     * @param health of the unit
     * @param attack of the unit
     * @param armour of the unit
     */
    public CavalryUnit(String name, int health, int attack, int armour) {
        super(name, health, attack, armour);
    }




    /**
     * Simplified constructor extended from super- class Unit. Gives the unit attack 20 and and armour of 12
     *
     * @param name   of the unit
     * @param health of the unit
     */
    public CavalryUnit(String name, int health) {
        super(name, health, 20, 12);
    }




    /**
     * Method that gives the unit an attack bonus in meele
     *
     * @return returns 4 in attack bonus and a default value of 2 afterwards
     */
    int attacks;

    @Override
    public int getAttackBonus() {
        attacks++;
        switch (attacks) {
            case 1: {
                if(this.getTerrain()==Terrain.PLAINS){
                    return 5;
                }
                else {
                    return 4;
                }
            }
            default: {
                if(this.getTerrain()==Terrain.PLAINS){
                    return 3;
                }
                else{
                return 2;
            }}
        }


    }



    /**
     * method that returns resist bonus
     *
     * @return resist bonus
     */
    @Override
    public int getResistBonus() {
        if(this.getTerrain()==Terrain.FOREST){
            return 0;
        }
        return 1;
    }

    @Override
    public UnitEnum getUnitType() {
        return UnitEnum.CAVALRY;
    }
}