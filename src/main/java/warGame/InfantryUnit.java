package warGame;

public class InfantryUnit extends Unit {



    /**
     * Constructor extended from super- class Unit
     *
     * @param name   of the unit
     * @param health of the unit
     * @param attack of the unit
     * @param armour of the unit
     */

    public InfantryUnit(String name, int health, int attack, int armour) {
        super(name, health, attack, armour);
    }




    /**
     * Simplified constructor from the extended super class Unit,gives the unit attack 10 and armour of 10
     *
     * @param name   of the unit
     * @param health of the unit
     */
    public InfantryUnit(String name, int health) {
        super(name, health, 15, 10);


    }




    /**
     * Method that gives the unit  a strength of 2 in melee
     *
     * @return the strength of 2 in melee
     */
    @Override
    public int getAttackBonus() {
        if(this.getTerrain()==Terrain.FOREST){
            return 4;
        }

        return 2;
    }





    /**
     * Method that gives the unit a resist bonus in fights
     *
     * @return the resist bonus of 1
     */

    @Override
    public int getResistBonus() {
        if(this.getTerrain()==Terrain.FOREST){
            return 4;}
        return 1;
    }

    @Override
    public UnitEnum getUnitType() {
        return UnitEnum.INFANTRY;
    }
}
