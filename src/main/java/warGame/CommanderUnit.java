package warGame;

public class CommanderUnit extends CavalryUnit {


    /**
     * Constructor extended from CavalaryUnit
     *
     * @param name   of the unit
     * @param health of the unit
     * @param attack of the unit
     * @param armour of the unit
     */


    public CommanderUnit(String name, int health, int attack, int armour) {
        super(name, health, attack, armour);
    }



    /**
     * Simplified constructor extended from commanderUnit. Gives the unit attack of 25 and armour of 15
     *
     * @param name   of the unit
     * @param health of the unit
     */
    public CommanderUnit(String name, int health) {
        super(name, health, 25, 15);
    }

    @Override
    public UnitEnum getUnitType() {
        return UnitEnum.COMMANDER;
    }
}

