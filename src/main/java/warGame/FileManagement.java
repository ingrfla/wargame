package warGame;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.*;

public class FileManagement {
    public static void saveToFile(String information, Path path){
        if(!Files.exists(path)){
            File file = new File(String.valueOf(path));
        }
try{
    Files.writeString(path,information, StandardCharsets.UTF_8);

}
catch (IOException e) {
    e.printStackTrace();
}
    }


    public static void writeArmyToFile(Army army){
        if(army==null) throw new IllegalArgumentException("Army cannot be null");
      Path pathOfFile = Paths.get("src/main/resources/Files/"+ army.getName()+".csv");
      String information = army.getName()+"\n";
      for(Unit unit : army.getUnits()){
          information += unit.getUnitType()+ "," + unit.getName() + "," + unit.getHealth()+ "\n";
      }
      saveToFile(information,pathOfFile);

    }

    public Army readArmy(File file) throws IOException {
        if (!file.getPath().toLowerCase(Locale.ROOT).endsWith(".csv")){
            throw new IOException("This file is not csv");
        }
        String armyName = file.getName().replace(".csv","");
        Army army = new Army(armyName);
        UnitFactory factory = new UnitFactory();

        try (Scanner scan = new Scanner(file)) {
            while (scan.hasNextLine()) {
                String iterateLine = scan.nextLine();

                if (!iterateLine.contains(",")) {
                    army.setName(iterateLine);
                }
                else {
                    List<String> info = Arrays.asList(iterateLine.split(","));
                    switch (info.get(0)) {
                        case "CAVALRY" -> army.addUnit(factory.makeUnit(UnitEnum.CAVALRY,info.get(1),Integer.parseInt(info.get(2))));
                        case "COMMANDER"->army.addUnit(factory.makeUnit(UnitEnum.COMMANDER,info.get(1),Integer.parseInt(info.get(2))));
                        case "INFANTRY" -> army.addUnit(factory.makeUnit(UnitEnum.INFANTRY,info.get(1),Integer.parseInt(info.get(2))));
                        case "RANGED" -> army.addUnit(factory.makeUnit(UnitEnum.RANGED,info.get(1),Integer.parseInt(info.get(2))));
                        default -> System.out.println("Error");
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return army;
    }
}









