package warGame;

import java.util.ArrayList;

public class UnitFactory {


    public Unit makeUnit(UnitEnum unitType,String name, int health)
    {

            switch (unitType) {
                case CAVALRY:
                   return  new CavalryUnit(name, health);

                case COMMANDER:
                    return new CommanderUnit(name, health);

                case INFANTRY:
                    return new InfantryUnit(name, health);
                case RANGED:
                     return new RangedUnit(name, health);


                default:throw new IllegalArgumentException();
            }}

public ArrayList<Unit> listOfUnits( int amountOfUnits,UnitEnum unitType, String nameOfUnit, int health){
        ArrayList<Unit> listOfUnits  = new ArrayList<>();
        for (int i = 0;i<amountOfUnits;i++){
            listOfUnits.add(makeUnit(unitType,nameOfUnit,health));
        }
        return listOfUnits;
    }
}

