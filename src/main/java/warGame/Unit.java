package warGame;


public abstract class Unit {
    private String name;
    private int health;
    private int attack;
    private int armour;
    private Terrain terrain;






    /**
     * constructor that creates an unit
     * @param name of the unit
     * @param health of the unit
     * @param attack of the unit
     * @param armour of the unit
     */
    public Unit(String name, int health, int attack, int armour) { {
        }
        this.name = name;
        if(this.name.isBlank()){
            throw new IllegalArgumentException("Name cannot be null");
        }
        this.health = health;
    if(health<0){
            throw new IllegalArgumentException("Health must be over 0");
        }
        this.attack = attack;
        this.armour = armour;
    }




    /**
     * Constructor that creates an unit based on name and health
     * @param name of the unit
     * @param health of the unit
     */
    public Unit(String name, int health) {
        this.name=name;
        this.health=health;
    }



    /**
     * name method
     * @return the name og the unit
     */

    public String getName() {
        return name;
    }




    /**
     * health method
     * @return the health of the unit
     */

    public int getHealth() {
        return health;
    }




    /**
     * attack method
     * @return the attack of the weapon of the unit
     */
    public int getAttack() {
        return attack;
    }



    /**
     * armour method
     * @return the defense value of the armour
     */
    public int getArmour() {
        return armour;
    }




    /**
     *Method that gives bonus in attacks. Added when the unit attacks another unit
     * @return attack bonus
     */
    public  abstract int getAttackBonus();




    /**
     * Method that gives bonus in defense. Added when the unit attacks another unit
     * @return defense bonus
     */
    public abstract int getResistBonus();





    /**
     * Method that changes the health(if attacked)
     * @param health
     */

    public void setHealth(int health) {
        this.health = health;
    }





    /**
     * Method that attacks another opponent
     * @param opponent gets attacked
     */

    public void attack(Unit opponent){
        int health = opponent.health - (this.attack + this.getAttackBonus()) + (opponent.getArmour() + getResistBonus());
        opponent.setHealth(health);
    }



    public void setTerrain(Terrain terrain){
        if(!(terrain == null)){
            this.terrain = terrain;
        }else{
            throw new NullPointerException("Terrain type cannot be null");
        }
    }
    public Terrain getTerrain(){
        return terrain;
    }


    /**
     * Tostring that informs about the current situation of an unit
     * @return Units name along with health, attack and armour
     */

    public String InfantaryToString() {
        return
                "InfantaryUnit" + name + "," + health;
    }

    public String unitToString() {
        return
                "\n" +  getClass().getSimpleName() + "," +   name + "," + health  ;
    }

    public abstract UnitEnum getUnitType();
}
