package warGame;

public class RangedUnit extends Unit {



    /**
     * Constructor extended from super- class Unit
     *
     * @param name   of the unit
     * @param health of the unit
     * @param attack of the unit
     * @param armour of the unit
     */

    public RangedUnit(String name, int health, int attack, int armour) {
        super(name, health, attack, armour);
    }




    /**
     * Simplified constructor extended from super- class Unit, gives the unit attack 15 and armour of 8
     *
     * @param name   of the unit
     * @param health of the unit
     */
    public RangedUnit(String name, int health) {
        super(name, health, 15, 8);
    }

    int attacks;




    /**
     * Method that gives the unit has a strength of 3 in melee
     *
     * @return the strength 0f 3 in melee
     */
    @Override
    public int getAttackBonus() {
        if(this.getTerrain()==Terrain.HILL){
            return 5;
        }
        if(this.getTerrain()==Terrain.FOREST){
            return 2;
        }
        return 3;
    }





    /**
     * Method that gives the unit a resist bonus in fights
     *
     * @return the resist bonus of 6, then 4 and a default value of 2 afterwards
     */
    @Override
    public int getResistBonus() {
        attacks++;
        switch (attacks) {
            case 1: {
                return 6;
            }
            case 2: {
                return 4;

            }
            default: {
                return 2;
            }

        }
    }

    @Override
    public UnitEnum getUnitType() {
        return UnitEnum.RANGED;
    }


}
