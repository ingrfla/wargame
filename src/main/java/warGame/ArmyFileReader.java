package warGame;

import java.io.*;

public class ArmyFileReader {

    public void readFile(File file) throws Exception {
        try {
            BufferedReader br = null;
            FileReader fr = null;

            fr = new FileReader(file);
            br = new BufferedReader(fr);
            String sCurrentLine = "";

            while ((sCurrentLine = br.readLine()) != null) {
                System.out.println(sCurrentLine);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if(file.getName().trim().isBlank()){
            try {
                throw new Exception("Can not read from a file without name.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(file.length() == 0){
            try {
                throw new Exception("The given file is empty");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }



    public void copyFile(File file, File file1) {
        try {
            BufferedWriter bw = new BufferedWriter(new FileWriter(file1));
            BufferedReader br = new BufferedReader(new FileReader(file));
            String s;
            while ((s = br.readLine()) != null) {
                bw.write(s + "\n");
                System.out.println(s);

            }
            br.close();
            bw.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
        if (file.getName().trim().isBlank()) {
            try {
                throw new Exception("Can not read from a file without name.");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if (file.length() == 0) {
            try {
                throw new Exception("The given file is empty");
            } catch (Exception e) {
                e.printStackTrace();
            }
        }}
}






