package warGame;

import java.io.*;
import java.nio.file.Paths;
import java.util.List;

public class ArmyFileWriter {


    public static PrintStream printWriter;

    public static void writeToFile(String s, File file)  {

        try (FileWriter fileWriter = new FileWriter(file)){
             BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
             PrintWriter printWriter = new PrintWriter(bufferedWriter);
             printWriter.write(s);
             printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
