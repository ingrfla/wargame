package JavaFx;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.*;
import warGame.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ChooseArmy extends ChildController {

    @FXML
    public ToggleGroup armyTwo;
    @FXML
    public TextField nameOfArmyOne, nameOfArmyTwo, nameOfUnit;
    @FXML
    public Spinner<Integer> healthOfUnit, amountOfUnits;
    @FXML
    public RadioButton infantryUnit, commanderUnit, cavlaryUnit, rangedUnit;
    @FXML
    public TextArea unitInformation;
    @FXML
    Button  nextArmy, addUnit,confirm;
    @FXML
    public RadioButton plains, hill, forest;
    @FXML
    public TextField terrainInfo,battleInfo;

    UnitFactory factory = new UnitFactory();
    FileManagement fileManagement = new FileManagement();

    private Army currentArmy1;
    private Army currentArmy2;
    private Army winnerChoose;
    private SpinnerValueFactory<Integer> maxUnits,health ;
    private List<Unit> units = new ArrayList<>();




    @Override
    public void load() {
         maxUnits = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 100);
        amountOfUnits.setValueFactory(maxUnits);
       health = new SpinnerValueFactory.IntegerSpinnerValueFactory(1, 100);
        healthOfUnit.setValueFactory(health);
        unitInformation.setText(" No units in army");
       this.currentArmy1= new Army("army One");
        this.currentArmy2= new Army("army two");
       infantryUnit.setSelected(true);
        parent.currentArmyOne = this.currentArmy1;
        parent.currentArmyTwo = this.currentArmy2;


    }

    @FXML
    public void nextArmy(ActionEvent event) {
            parent.show("ChooseArmyTwo.fxml");
        }



    public UnitEnum unitType() {
        if (infantryUnit.isSelected()) {
            return UnitEnum.INFANTRY;

        } else if (commanderUnit.isSelected()) {
            return UnitEnum.COMMANDER;

        } else if (cavlaryUnit.isSelected()) {
            return UnitEnum.CAVALRY;

        } else if (rangedUnit.isSelected()) {
            return UnitEnum.RANGED;
        }
        return null;
    }


    public void fillUnitOne() {
        String name = nameOfUnit.getText();
        int healthUnit = healthOfUnit.getValue();
        int amounOfUnit = amountOfUnits.getValue();
        reset();
        UnitEnum type = unitType();
        unitInformation.setText(unitType() + "\n" + "Name of Unit: " + name + "\n" + "Unit health:  " + healthUnit + "\n" + "Amount:  " + amounOfUnit);
        System.out.println(unitType());
        System.out.println(currentArmy1);
        units = factory.listOfUnits(amounOfUnit,type, name, healthUnit);
        this.currentArmy1.addUnits(units);
        this.currentArmy1= parent.currentArmyOne;
        System.out.println(this.currentArmy1);
    }


    public void fillUnitTwo() {

        String name = nameOfUnit.getText();
        int healthUnit = healthOfUnit.getValue();
        int amounOfUnit = amountOfUnits.getValue();
        reset();
        UnitEnum type = unitType();
        unitInformation.setText(unitType() + "\n" + "Name of Unit: " + name + "\n" + "Unit health:  " + healthUnit + "\n" + "Amount:  " + amounOfUnit);
        System.out.println(unitType());
        units = factory.listOfUnits(amounOfUnit,type, name, healthUnit);
        currentArmy2.addUnits(units);
        this.currentArmy2 = parent.currentArmyTwo;

    }

    public void reset(){
        nameOfUnit.clear();
        maxUnits.setValue(1);
        health.setValue(1);

    }


    public void armyMaker1(ActionEvent event) throws IOException {
        System.out.println(this.currentArmy1);
            parent.currentArmyOne.setName(nameOfArmyOne.getText());
        this.currentArmy1 = parent.currentArmyTwo;
            unitInformation.setText(nameOfArmyOne.getText() +   "Amount of unit   " + parent.currentArmyOne.getUnits().size());
            fileManagement.writeArmyToFile(currentArmy1);



        }


    public void armyMaker2(ActionEvent event) {
       parent.currentArmyTwo.setName(nameOfArmyTwo.getText());
        this.currentArmy2 =parent.currentArmyTwo ;
        unitInformation.setText(nameOfArmyTwo.getText() +   "Amount of unit   " + parent.currentArmyOne.getUnits().size());
        fileManagement.writeArmyToFile(currentArmy2);

    }
    @FXML
    public void toTerrain() {
        parent.show("TerrainMakeArmy.fxml");

    }


    public Terrain getTerrain() {

        if (plains.isSelected()) {
           terrainInfo.setText("Selected terrain:              " + String.valueOf(Terrain.PLAINS));
            return Terrain.PLAINS;
        }
        if (hill.isSelected()) {
           terrainInfo.setText("Selected terrain:            " + String.valueOf(Terrain.HILL));
            return Terrain.HILL;
        }
        if (forest.isSelected()) {
           terrainInfo.setText("Selected terrain:              " + String.valueOf(Terrain.FOREST));
            return Terrain.FOREST;
        }
        return null;
    }


    public void toBattle() {
        parent.show("BattleMakeArmt.fxml");

    }


    public void simulationChooseArmy() {

        for (Unit unit : this.currentArmy2.getUnits()) {
            unit.setTerrain(getTerrain());
        }
        for (Unit unit : this.currentArmy1.getUnits()) {
            unit.setTerrain(getTerrain());
        }
        this.currentArmy1 = parent.currentArmyOne;
        this.currentArmy2 = parent.currentArmyTwo;
        Battle battle = new Battle(this.currentArmy1, this.currentArmy2, getTerrain());
        winnerChoose = battle.simulate();
        battleInfo.setText(( "Winner :" +  winnerChoose.getName() + "CAVLARY:     " +  winnerChoose.getCavlaryUnits().size()) + "COMMANDR:           " +  winnerChoose.getCommanderUnit().size() + "INFANTRY:       " + winnerChoose.getInfantaryUnit().size() + "RAMGED      " + winnerChoose.getRangedUnits().size());
    }
}











