package JavaFx;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.scene.input.MouseEvent;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import warGame.*;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import java.util.Scanner;

public class ArmyFromFile extends ChildController implements Initializable {
    @FXML
    public RadioButton armyOneFile, armyTwoFile;
    @FXML
    public Button openFile, confirm;
    @FXML
    ToggleGroup fromFile;
    @FXML
    public Button Battle;
    @FXML
    public RadioButton plains, hill, forest;
    @FXML
    public TextField terrainInfo;

    public TextArea textArea, battleInfo;
    public TextField filePath;
    private Army currentArmyOneFile, currentArmyTwoFile;
    private Army winnerFromFile;


    FileChooser fileChooser = new FileChooser();
    FileManagement fileManagement = new FileManagement();

    private File file;


    @Override
    public void load() {

        this.currentArmyOneFile = parent.currentArmyOne;
        this.currentArmyTwoFile = parent.currentArmyTwo;

    }

    @FXML
    public void getChoice(ActionEvent event) {

        if (armyOneFile.isSelected()) {
            parent.show("ArmyOneFile.fxml");
        } else if (armyTwoFile.isSelected()) {
            parent.show("ArmyTwoFile.fxml");
        }
    }


    public void getText(MouseEvent event) throws FileNotFoundException {
        file = fileChooser.showOpenDialog(new Stage());
        System.out.println(file.getPath());
        //System.out.println(fileChooser.showOpenDialog(new Stage()));
        Scanner scanner = new Scanner(file);
        while (scanner.hasNext()) {
            textArea.appendText(scanner.nextLine() + "\n");
        }
    }

    @Override
    public void initialize(URL url, ResourceBundle resourceBundle) {
        fileChooser.setInitialDirectory(new File("src/main/resources/Files"));
        // if(!textArea.getText().isBlank()){
        //  confirm.setDisable(false);

        // }
        // else{
        //  confirm.setDisable(true);
        // }
    }

    public void getPathFile() {
        filePath.setText("Path of File:" + file.getPath());
    }


    public void makeArmyOne() throws IOException {
        parent.currentArmyOne = fileManagement.readArmy(file);
        this.currentArmyOneFile = parent.currentArmyOne;
        System.out.println(this.currentArmyOneFile.getUnits().size());
        getPathFile();

    }

    public void makeArmyTwo() throws IOException {
        parent.currentArmyTwo = fileManagement.readArmy(file);
        this.currentArmyTwoFile = parent.currentArmyTwo;
        System.out.println(this.currentArmyTwoFile.getUnits().size());
        getPathFile();


    }


    @FXML
    public void Battle() {
        parent.show("TerrainArmyFromFile.fxml");

    }

    public Terrain getTerrain() {

        if (plains.isSelected()) {
            terrainInfo.setText("Selected terrain:     " + String.valueOf(Terrain.PLAINS));
            return Terrain.PLAINS;
        }
        if (hill.isSelected()) {
            terrainInfo.setText("Selected terrain:    " + String.valueOf(Terrain.HILL));
            return Terrain.HILL;
        }
        if (forest.isSelected()) {
            terrainInfo.setText("Selected terrain:       " + String.valueOf(Terrain.FOREST));
            return Terrain.FOREST;
        }
        return null;
    }


    public void simulationFromFile() {


        for (Unit unit : this.currentArmyOneFile.getUnits()) {
            unit.setTerrain(getTerrain());
        }
        for (Unit unit : this.currentArmyTwoFile.getUnits()) {
            unit.setTerrain(getTerrain());
        }
        Battle battle = new Battle(this.currentArmyOneFile, this.currentArmyTwoFile, getTerrain());
        winnerFromFile = battle.simulate();
       terrainInfo.setText(( "Winner :" +  winnerFromFile.getName() + "CAVLARY:     " +  winnerFromFile.getCavlaryUnits().size()) + "COMMANDR:           " +  winnerFromFile.getCommanderUnit().size()+ "INFANTRY:       " + winnerFromFile.getInfantaryUnit().size() + "RANGED     "  + winnerFromFile.getRangedUnits().size());
    }
}




