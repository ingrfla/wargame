package JavaFx;

import javafx.application.HostServices;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.layout.BorderPane;
import warGame.Army;

import java.io.IOException;

public class MainController {

    @FXML
    public BorderPane scenePane;
    protected Army currentArmyOne;
    protected Army currentArmyTwo;
    HostServices hostServices;


    /**
     *  NB: PARTS OF MAINCONTROLLER IS FROM SYSTEMUTVIKLIN
     * Method to show a given .fxml file when executed.
     * @param filename the .fxml file that will be shown.
     */
    public void show(String filename) {
        FXMLLoader loader;

        try {
            loader = new FXMLLoader(getClass().getClassLoader().getResource(filename));
            scenePane.setCenter(loader.load());
        } catch (IOException ioe) {
            System.out.println("Failed to open child document, skipping");
            System.out.println(ioe);
            ioe.printStackTrace();
            return;
        }

        try {
            ChildController childController = loader.getController();
            childController.initialize(this);
        } catch (NullPointerException npe) {
            System.out.println("Failed to load child controller, skipping");
            System.out.println(npe);
            npe.printStackTrace();
        }
    }
}
