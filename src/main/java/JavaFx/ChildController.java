package JavaFx;

/**
 * NB: FROM SYSTEMUTVIKLING
 *
 */

public abstract class ChildController {

    protected MainController parent;

    abstract public void load();

    /**
     * initialize maincontroller
     * @param parent
     */

    public void initialize(MainController parent) {
        this.parent = parent;
        this.load();
    }
}
